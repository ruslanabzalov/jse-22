package tsc.abzalov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.ObjectUtils.allNotNull;
import static tsc.abzalov.tm.enumeration.CommandType.SYSTEM_COMMAND;

public final class SystemHelpCommand extends AbstractCommand {

    public SystemHelpCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "help";
    }

    @Override
    @NotNull
    public String getCommandArgument() {
        return "-h";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Shows all available commands.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SYSTEM_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final List<AbstractCommand> commandsList =
                new ArrayList<>(getServiceLocator().getCommandService().getCommands());
        commandsList.sort((first, second) -> {
            final int firstOrdinal = first.getCommandType().ordinal();
            final int secondOrdinal = second.getCommandType().ordinal();
            if (firstOrdinal == secondOrdinal) return 0;
            if (firstOrdinal > secondOrdinal) return firstOrdinal - secondOrdinal;
            return secondOrdinal - firstOrdinal;
        });
        @NotNull String helpMessage = formatHelpMessage(commandsList);
        System.out.print(helpMessage);
    }

    /**
     * Format message for 'help [-h]' command.
     * @param commands Application commands.
     * @return Formatted 'help [-h]' command message.
     * @author Ruslan Abzalov.
     */
    @NotNull
    private String formatHelpMessage(@NotNull List<AbstractCommand> commands) {
        @NotNull final StringBuilder builder = new StringBuilder();
        @Nullable AbstractCommand currentCommand;
        @Nullable AbstractCommand previousCommand;
        for (int i = 0; i < commands.size(); i++) {
            currentCommand = commands.get(i);
            previousCommand = (i == 0) ? currentCommand : commands.get(i - 1);
            if (allNotNull(currentCommand, previousCommand)) {
                if (i == 0 || !currentCommand.getCommandType().equals(previousCommand.getCommandType())) {
                    builder
                            .append("\n[")
                            .append(currentCommand.getCommandType().getDisplayName().toUpperCase())
                            .append("]\n");
                }
                builder.append(currentCommand).append("\n");
            }
        }
        return builder.append("\n").toString();
    }

}
