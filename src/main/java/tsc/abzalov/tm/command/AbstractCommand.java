package tsc.abzalov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.enumeration.Role;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static tsc.abzalov.tm.enumeration.CommandType.ADMIN_COMMAND;
import static tsc.abzalov.tm.enumeration.Role.ADMIN;
import static tsc.abzalov.tm.enumeration.Role.USER;

public abstract class AbstractCommand {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private final List<Role> roles = new ArrayList<>();

    public AbstractCommand(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        roles.add(ADMIN);
        if (!getCommandType().equals(ADMIN_COMMAND)) roles.add(USER);
    }

    @NotNull
    public IServiceLocator getServiceLocator() {
        return this.serviceLocator;
    }

    @NotNull
    public List<Role> getRoles() {
        return roles;
    }

    @NotNull
    public abstract String getCommandName();

    @Nullable
    public abstract String getCommandArgument();

    @NotNull
    public abstract String getDescription();

    @NotNull
    public abstract CommandType getCommandType();

    public abstract void execute() throws Exception;

    @Override
    @NotNull
    public String toString() {
        @NotNull final String correctArg = (isBlank(getCommandArgument()))
                ? ": "
                : " [" + getCommandArgument() + "]: ";
        return getCommandName() + correctArg + getDescription() + " " + Arrays.toString(getRoles().toArray());
    }

}
