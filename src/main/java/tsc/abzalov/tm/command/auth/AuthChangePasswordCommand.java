package tsc.abzalov.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.auth.UserIsNotExistException;
import tsc.abzalov.tm.model.User;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.AUTH_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputPassword;

public final class AuthChangePasswordCommand extends AbstractCommand {

    public AuthChangePasswordCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "change-password";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Change current user password.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return AUTH_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("PASSWORD CHANGING\n");
        @NotNull final String newPassword = inputPassword();
        @NotNull final String currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        @Nullable final User updatedUser = getServiceLocator().getUserService().editPasswordById(currentUserId, newPassword);
        Optional.ofNullable(updatedUser).orElseThrow(() -> new UserIsNotExistException(currentUserId));
        System.out.println("Password successfully changed.");
    }

}
