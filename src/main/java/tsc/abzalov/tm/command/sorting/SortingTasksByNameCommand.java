package tsc.abzalov.tm.command.sorting;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.model.Task;

import java.util.List;

import static tsc.abzalov.tm.enumeration.CommandType.SORTING_COMMAND;

public final class SortingTasksByNameCommand extends AbstractCommand {

    public SortingTasksByNameCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "sort-tasks-by-name";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Sort tasks by name.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return SORTING_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ALL TASKS LIST SORTED BY NAME\n");
        @NotNull final ITaskService taskService = getServiceLocator().getTaskService();
        @NotNull final String currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        final boolean areTasksExist = taskService.size(currentUserId) != 0;
        if (areTasksExist) {
            @NotNull final List<Task> tasks = taskService.sortByName(currentUserId);
            for (@NotNull final Task task : tasks)
                System.out.println((taskService.indexOf(currentUserId, task) + 1) + ". " + task);
            System.out.println();
            return;
        }
        System.out.println("Tasks list is empty.\n");
    }

}
