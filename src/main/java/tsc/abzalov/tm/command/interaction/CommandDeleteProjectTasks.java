package tsc.abzalov.tm.command.interaction;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.util.InputUtil;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.INTERACTION_COMMAND;

public final class CommandDeleteProjectTasks extends AbstractCommand {

    public CommandDeleteProjectTasks(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "delete-project-with-tasks";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete project with tasks.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return INTERACTION_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DELETE PROJECTS WITH TASKS\n");
        @NotNull final IProjectTaskService projectTasksService = getServiceLocator().getProjectTaskService();
        @NotNull final String currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        if (projectTasksService.hasData(currentUserId)) {
            System.out.println("Project");
            @NotNull final String projectId = InputUtil.inputId();
            System.out.println();
            if (!Optional.ofNullable(projectTasksService.findProjectById(currentUserId, projectId)).isPresent()) {
                System.out.println("Project was not found.\n");
                return;
            }
            projectTasksService.deleteProjectTasksById(currentUserId, projectId);
            projectTasksService.deleteProjectById(currentUserId, projectId);
            System.out.println("Project and it tasks was successfully deleted.\n");
            return;
        }
        System.out.println("One of the lists is empty!");
    }

}
