package tsc.abzalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.model.Project;

import java.util.List;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;

public final class ProjectShowAllCommand extends AbstractCommand {

    public ProjectShowAllCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "show-all-projects";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ALL PROJECTS LIST\n");
        @NotNull final IProjectService projectService = getServiceLocator().getProjectService();
        @NotNull final String currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        final boolean areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            @NotNull final List<Project> projects = projectService.findAll(currentUserId);
            for (@NotNull final Project project : projects)
                System.out.println((projectService.indexOf(currentUserId, project) + 1) + ". " + project);
            System.out.println();
            return;
        }
        System.out.println("Projects list is empty.\n");
    }

}
