package tsc.abzalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.model.Task;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputIndex;

public final class TaskShowByIndexCommand extends AbstractCommand {

    public TaskShowByIndexCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "show-task-by-index";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show task by index.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("FIND TASK BY INDEX\n");
        @NotNull final ITaskService taskService = getServiceLocator().getTaskService();
        @NotNull final String currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        final boolean areTasksExist = taskService.size(currentUserId) != 0;
        if (areTasksExist) {
            final int taskIndex = inputIndex();
            System.out.println();
            @Nullable final Task task = taskService.findByIndex(currentUserId, taskIndex);
            if (!Optional.ofNullable(task).isPresent()) {
                System.out.println("Searched task was not found.\n");
                return;
            }
            System.out.println((taskService.indexOf(currentUserId, task) + 1) + ". " + task + "\n");
            return;
        }
        System.out.println("Tasks list is empty.\n");
    }

}
