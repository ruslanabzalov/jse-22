package tsc.abzalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.model.Task;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.*;

public final class TaskUpdateByIndexCommand extends AbstractCommand {

    public TaskUpdateByIndexCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "update-task-by-index";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Update task by index.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("EDIT TASK BY INDEX\n");
        @NotNull final ITaskService taskService = getServiceLocator().getTaskService();
        @NotNull final String currentUserId = getServiceLocator().getAuthService().getCurrentUserId();
        final boolean areTasksExist = taskService.size(currentUserId) != 0;
        if (areTasksExist) {
            final int taskIndex = inputIndex();
            @NotNull final String taskName = inputName();
            @NotNull final String taskDescription = inputDescription();
            System.out.println();
            @Nullable final Task task =
                    taskService.editByIndex(currentUserId, taskIndex, taskName, taskDescription);
            if (!Optional.ofNullable(task).isPresent()) {
                System.out.println("Task was not updated! Please, check that task exists and try again.");
                return;
            }
            System.out.println("Task was successfully updated.");
            return;
        }
        System.out.println("Tasks list is empty.\n");
    }

}
