package tsc.abzalov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.auth.CannotDeleteCurrentUserException;

import static tsc.abzalov.tm.util.InputUtil.inputId;

public final class UserDeleteByIdCommand extends AbstractCommand {

    public UserDeleteByIdCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "delete-user-by-id";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete user by id.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return CommandType.ADMIN_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("DELETE USER BY ID");
        @NotNull final String userId = inputId();
        if (userId.equals(getServiceLocator().getAuthService().getCurrentUserId()))
            throw new CannotDeleteCurrentUserException();
        getServiceLocator().getUserService().removeById(userId);
        System.out.println("User Successfully Deleted By Admin.");
    }

}
