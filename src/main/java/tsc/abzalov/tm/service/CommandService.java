package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.service.ICommandService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.exception.general.IncorrectCommandException;

import java.util.Collection;
import java.util.Optional;

@SuppressWarnings("ResultOfMethodCallIgnored")
public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    @NotNull
    public Collection<String> getCommandNames() {
        return commandRepository.getCommandNames();
    }

    @Override
    @NotNull
    public Collection<String> getCommandArguments() {
        return commandRepository.getCommandArguments();
    }

    @Override
    @NotNull
    public AbstractCommand getCommandByName(@NotNull final String name) throws IncorrectCommandException {
        Optional.of(name).orElseThrow(() -> new IncorrectCommandException(name));
        return commandRepository.getCommandByName(name);
    }

    @Override
    @NotNull
    public AbstractCommand getArgumentByName(@NotNull final String name) throws IncorrectCommandException {
        Optional.of(name).orElseThrow(() -> new IncorrectCommandException(name));
        return commandRepository.getArgumentByName(name);
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        commandRepository.add(command);
    }

}
