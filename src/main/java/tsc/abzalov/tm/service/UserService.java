package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.exception.auth.EmptyEmailException;
import tsc.abzalov.tm.exception.auth.EmptyFirstNameException;
import tsc.abzalov.tm.exception.auth.IncorrectCredentialsException;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.model.User;

import java.util.Optional;

import static tsc.abzalov.tm.util.HashUtil.hash;

@SuppressWarnings("ResultOfMethodCallIgnored")
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public void create(
            @NotNull final String login, @NotNull final String password,
            @NotNull final Role role, @NotNull final String firstName,
            @Nullable final String lastName, @NotNull final String email
    ) throws Exception {
        checkMainInfo(login, password, firstName, email);
        this.create(new User(login, hash(password), role, firstName, lastName, email));
    }

    @Override
    public void create(
            @NotNull final String login, @NotNull final String password,
            @NotNull final String firstName, @Nullable final String lastName,
            @NotNull final String email
    ) throws Exception {
        checkMainInfo(login, password, firstName, email);
        this.create(new User(login, hash(password), firstName, lastName, email));
    }

    @Override
    @Nullable
    public User findByLogin(@NotNull final String login) throws Exception {
        Optional.of(login).orElseThrow(IncorrectCredentialsException::new);
        return Optional.ofNullable(userRepository.findByLogin(login))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    public User findByEmail(@NotNull final String email) throws Exception {
        Optional.of(email).orElseThrow(EmptyEmailException::new);
        return Optional.ofNullable(userRepository.findByEmail(email))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    public User editPasswordById(@NotNull final String id, @NotNull final String newPassword) throws Exception {
        Optional.of(id).orElseThrow(EmptyIdException::new);
        Optional.of(newPassword).orElseThrow(IncorrectCredentialsException::new);
        return Optional.ofNullable(userRepository.editPassword(id, hash(newPassword)))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    public User editUserInfoById(
            @NotNull final String id, @NotNull final String firstName, @Nullable final String lastName
    ) throws Exception {
        Optional.of(id).orElseThrow(EmptyIdException::new);
        Optional.of(firstName).orElseThrow(EmptyFirstNameException::new);
        return Optional.ofNullable(userRepository.editUserInfo(id, firstName, lastName))
                .orElseThrow(EntityNotFoundException::new);
    }

    private void checkMainInfo(
            @NotNull final String login, @NotNull final String password,
            @NotNull final String firstName, @NotNull final String email
    ) throws Exception {
        Optional.of(login).orElseThrow(IncorrectCredentialsException::new);
        Optional.of(password).orElseThrow(IncorrectCredentialsException::new);
        Optional.of(firstName).orElseThrow(EmptyFirstNameException::new);
        Optional.of(email).orElseThrow(EmptyEmailException::new);
    }

    @Override
    public void deleteByLogin(@NotNull final String login) throws Exception {
        Optional.of(login).orElseThrow(IncorrectCredentialsException::new);
        userRepository.deleteByLogin(login);
    }

    @Override
    @Nullable
    public User lockUnlockById(@NotNull final String id) throws Exception {
        Optional.of(id).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(userRepository.lockUnlockById(id))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    public User lockUnlockByLogin(@NotNull final String login) throws Exception {
        Optional.of(login).orElseThrow(IncorrectCredentialsException::new);
        return Optional.ofNullable(userRepository.lockUnlockByLogin(login))
                .orElseThrow(EntityNotFoundException::new);
    }

}
