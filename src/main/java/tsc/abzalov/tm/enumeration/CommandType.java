package tsc.abzalov.tm.enumeration;

public enum CommandType {

    SYSTEM_COMMAND("System Command"),
    PROJECT_COMMAND("Project Command"),
    TASK_COMMAND("Task Command"),
    INTERACTION_COMMAND("Interaction Command"),
    SORTING_COMMAND("Sorting Command"),
    AUTH_COMMAND("Auth Command"),
    USER_COMMAND("User Command"),
    ADMIN_COMMAND("Admin Command");

    private final String displayName;

    CommandType(final String name) {
        this.displayName = name;
    }

    public String getDisplayName() {
        return this.displayName;
    }

}
