package tsc.abzalov.tm.enumeration;

public enum Role {

    ADMIN("Administrator"),
    USER("User");

    private final String displayName;

    Role(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
