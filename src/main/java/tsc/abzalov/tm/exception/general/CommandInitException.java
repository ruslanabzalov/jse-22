package tsc.abzalov.tm.exception.general;

import tsc.abzalov.tm.exception.AbstractException;

public final class CommandInitException extends AbstractException {

    public CommandInitException() {
        super("Command is not initialized!");
    }

}
