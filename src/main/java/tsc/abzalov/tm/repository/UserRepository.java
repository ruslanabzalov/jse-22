package tsc.abzalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.model.User;

import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final List<User> users = this.findAll();

    @Override
    @Nullable
    public User findByLogin(@NotNull final String login) {
        return users.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findByEmail(@NotNull final String email) {
        return users.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User editPassword(@NotNull final String id, @NotNull final String hashedPassword) {
        return users.stream()
                .filter(user -> id.equals(user.getId()))
                .findFirst()
                .map(user -> {
                    user.setHashedPassword(hashedPassword);
                    return user;
                })
                .orElse(null);
    }

    @Override
    @Nullable
    public User editUserInfo(
            @NotNull final String id, @NotNull final String firstName, @Nullable final String lastName
    ) {
        return users.stream()
                .filter(user -> id.equals(user.getId()))
                .findFirst()
                .map(user -> {
                    user.setFirstName(firstName);
                    user.setLastName(lastName);
                    return user;
                })
                .orElse(null);
    }

    @Override
    public void deleteByLogin(@NotNull final String login) {
        users.removeIf(user -> login.equals(user.getLogin()));
    }

    @Override
    @Nullable
    public User lockUnlockById(@NotNull final String id) {
        return users.stream()
                .filter(user -> id.equals(user.getId()))
                .findFirst()
                .map(user -> {
                    user.setLocked(!user.isLocked());
                    return user;
                })
                .orElse(null);
    }

    @Override
    @Nullable
    public User lockUnlockByLogin(@NotNull final String login) {
        return users.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .map(user -> {
                    user.setLocked(!user.isLocked());
                    return user;
                })
                .orElse(null);
    }

}
