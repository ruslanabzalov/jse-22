package tsc.abzalov.tm.model;

import org.jetbrains.annotations.NotNull;

public final class Project extends BusinessEntity {

    public Project(@NotNull final String name, @NotNull final String description, @NotNull final String userId) {
        super(name, description, userId);
    }

}
