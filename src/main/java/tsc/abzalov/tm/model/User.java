package tsc.abzalov.tm.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Role;

import java.util.Optional;

import static tsc.abzalov.tm.constant.LiteralConst.*;
import static tsc.abzalov.tm.enumeration.Role.USER;

public final class User extends AbstractEntity {

    @NotNull
    private String login;

    @NotNull
    private String hashedPassword;

    @NotNull
    private Role role = USER;

    @NotNull
    private String firstName;

    @Nullable
    private String lastName;

    @NotNull
    private String email;

    private boolean isLocked = false;

    public User(
            @NotNull final String login, @NotNull final String hashedPassword,
            @NotNull final Role role, @NotNull final String firstName,
            @Nullable final String lastName, @NotNull final String email
    ) {
        this.login = login;
        this.hashedPassword = hashedPassword;
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public User(
            @NotNull final String login, @NotNull final String hashedPassword,
            @NotNull final String firstName, @Nullable final String lastName,
            @NotNull final String email
    ) {
        this.login = login;
        this.hashedPassword = hashedPassword;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    @NotNull
    public String getLogin() {
        return login;
    }

    public void setLogin(@NotNull final String login) {
        this.login = login;
    }

    @NotNull
    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(@NotNull final String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    @NotNull
    public Role getRole() {
        return role;
    }

    public void setRole(@NotNull final Role role) {
        this.role = role;
    }

    @NotNull
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(@NotNull final String firstName) {
        this.firstName = firstName;
    }

    @Nullable
    public String getLastName() {
        return lastName;
    }

    public void setLastName(@Nullable final String lastName) {
        this.lastName = lastName;
    }

    @NotNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NotNull final String email) {
        this.email = email;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    @Override
    @NotNull
    public String toString() {
        @NotNull final String correctLastName = Optional.ofNullable(lastName).orElse(EMPTY_LAST_NAME);
        @NotNull final String correctUserStatus = (this.isLocked) ? LOCKED : ACTIVE;
        return "[ID: " + getId() +
                "; Login: " + this.login +
                "; Role: " + this.role.getDisplayName() +
                "; First Name: " + this.firstName +
                "; Last Name: " + correctLastName +
                "; Email: " + this.email +
                "; Status: " + correctUserStatus + "]";
    }

}
