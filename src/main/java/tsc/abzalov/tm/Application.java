package tsc.abzalov.tm;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.bootstrap.CommandBootstrap;

/**
 * Main application class.
 * @author Ruslan Abzalov.
 */
public final class Application {

    public static void main(@NotNull final String... args) {
        @NotNull final CommandBootstrap commandBootstrap = new CommandBootstrap();
        commandBootstrap.run(args);
    }

}
