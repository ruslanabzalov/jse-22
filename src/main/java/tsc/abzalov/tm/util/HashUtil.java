package tsc.abzalov.tm.util;

import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static java.security.MessageDigest.getInstance;

public final class HashUtil {

    private static final String ALGORITHM = "MD5";

    private static final int COUNTER = 128;

    private static final String SALT = "fwjojf23f2-323";

    @NotNull
    public static String hash(@NotNull String password) throws Exception {
        for (int i = 0; i < COUNTER; i++) password = md5(SALT + password + SALT);
        return password;
    }

    @NotNull
    private static String md5(@NotNull final String hash) throws Exception {
        try {
            @NotNull final MessageDigest md = getInstance(ALGORITHM);
            final byte[] bytes = md.digest(hash.getBytes());
            @NotNull StringBuilder builder = new StringBuilder();
            for (final byte b : bytes) builder.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            return builder.toString();
        } catch (NoSuchAlgorithmException exception) {
            throw new Exception(exception);
        }
    }

    private HashUtil() {
    }

}
