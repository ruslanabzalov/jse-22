package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Role;

public interface IAuthService {

    void register(
            @NotNull String login, @NotNull String password,
            @NotNull String firstName, @Nullable String lastName,
            @NotNull String email
    ) throws Exception;

    void login(@NotNull String login, @NotNull String password) throws Exception;

    void logoff() throws Exception;

    boolean IsSessionActive();

    boolean isSessionInactive();

    @NotNull
    String getCurrentUserId() throws Exception;

    @NotNull
    String getCurrentUserLogin() throws Exception;

    @NotNull
    Role getCurrentUserRole() throws Exception;

}
