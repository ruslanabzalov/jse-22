package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void info(@NotNull String message);

    void command(@NotNull String cmd);

    void error(@NotNull Exception exception);

}
