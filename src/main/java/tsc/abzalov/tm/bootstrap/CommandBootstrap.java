package tsc.abzalov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.*;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.command.auth.AuthChangePasswordCommand;
import tsc.abzalov.tm.command.auth.AuthLoginCommand;
import tsc.abzalov.tm.command.auth.AuthLogoffCommand;
import tsc.abzalov.tm.command.auth.AuthRegisterCommand;
import tsc.abzalov.tm.command.interaction.CommandAddTaskToProject;
import tsc.abzalov.tm.command.interaction.CommandDeleteProjectTask;
import tsc.abzalov.tm.command.interaction.CommandDeleteProjectTasks;
import tsc.abzalov.tm.command.interaction.CommandShowProjectTasks;
import tsc.abzalov.tm.command.project.*;
import tsc.abzalov.tm.command.sorting.*;
import tsc.abzalov.tm.command.system.*;
import tsc.abzalov.tm.command.task.*;
import tsc.abzalov.tm.command.user.*;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.exception.auth.UserIsNotExistException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.model.User;
import tsc.abzalov.tm.repository.CommandRepository;
import tsc.abzalov.tm.repository.ProjectRepository;
import tsc.abzalov.tm.repository.TaskRepository;
import tsc.abzalov.tm.repository.UserRepository;
import tsc.abzalov.tm.service.*;

import java.util.Arrays;
import java.util.List;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static tsc.abzalov.tm.enumeration.Role.ADMIN;
import static tsc.abzalov.tm.util.InputUtil.INPUT;

/**
 * Bootstrap application class.
 * @author Ruslan Abzalov.
 */
public final class CommandBootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    public CommandBootstrap() {
        registerCommand(new SystemAboutCommand(this));
        registerCommand(new SystemArgumentsCommand(this));
        registerCommand(new SystemCommandsCommand(this));
        registerCommand(new SystemExitCommand(this));
        registerCommand(new SystemHelpCommand(this));
        registerCommand(new SystemInfoCommand(this));
        registerCommand(new SystemVersionCommand(this));
        registerCommand(new ProjectCreateCommand(this));
        registerCommand(new ProjectDeleteAllCommand(this));
        registerCommand(new ProjectDeleteByIdCommand(this));
        registerCommand(new ProjectDeleteByIndexCommand(this));
        registerCommand(new ProjectDeleteByNameCommand(this));
        registerCommand(new ProjectEndByIdCommand(this));
        registerCommand(new ProjectShowAllCommand(this));
        registerCommand(new ProjectShowByIdCommand(this));
        registerCommand(new ProjectShowByIndexCommand(this));
        registerCommand(new ProjectShowByNameCommand(this));
        registerCommand(new ProjectStartByIdCommand(this));
        registerCommand(new ProjectUpdateByIdCommand(this));
        registerCommand(new ProjectUpdateByIndexCommand(this));
        registerCommand(new ProjectUpdateByNameCommand(this));
        registerCommand(new TaskCreateCommand(this));
        registerCommand(new TaskDeleteAllCommand(this));
        registerCommand(new TaskDeleteByIdCommand(this));
        registerCommand(new TaskDeleteByIndexCommand(this));
        registerCommand(new TaskDeleteByNameCommand(this));
        registerCommand(new TaskEndByIdCommand(this));
        registerCommand(new TaskShowAllCommand(this));
        registerCommand(new TaskShowByIdCommand(this));
        registerCommand(new TaskShowByIndexCommand(this));
        registerCommand(new TaskShowByNameCommand(this));
        registerCommand(new TaskStartByIdCommand(this));
        registerCommand(new TaskUpdateByIdCommand(this));
        registerCommand(new TaskUpdateByIndexCommand(this));
        registerCommand(new TaskUpdateByNameCommand(this));
        registerCommand(new CommandAddTaskToProject(this));
        registerCommand(new CommandDeleteProjectTask(this));
        registerCommand(new CommandDeleteProjectTasks(this));
        registerCommand(new CommandShowProjectTasks(this));
        registerCommand(new SortingProjectsByEndDateCommand(this));
        registerCommand(new SortingProjectsByNameCommand(this));
        registerCommand(new SortingProjectsByStartDateCommand(this));
        registerCommand(new SortingProjectsByStatusCommand(this));
        registerCommand(new SortingTasksByEndDateCommand(this));
        registerCommand(new SortingTasksByNameCommand(this));
        registerCommand(new SortingTasksByStartDateCommand(this));
        registerCommand(new SortingTasksByStatusCommand(this));
        registerCommand(new AuthRegisterCommand(this));
        registerCommand(new AuthLoginCommand(this));
        registerCommand(new AuthLogoffCommand(this));
        registerCommand(new AuthChangePasswordCommand(this));
        registerCommand(new UserShowInfoCommand(this));
        registerCommand(new UserChangeInfoCommand(this));
        registerCommand(new UserDeleteByIdCommand(this));
        registerCommand(new UserDeleteByLoginCommand(this));
        registerCommand(new UserLockUnlockByIdCommand(this));
        registerCommand(new UserLockUnlockByLoginCommand(this));
    }

    // Default users initialization.
    {
        try {
            userService.create("admin", "admin", ADMIN, "Admin", "", "admin@mail.com");
            @Nullable final User admin = userService.findByLogin("admin");
            if (admin == null) throw new UserIsNotExistException();
            initProject("Main Project", "Main Admin Project", admin.getId());
            initTask("Main Task", "Main Admin Task", admin.getId());

            userService.create("test", "test", "Test", "", "test@mail.com");
            @Nullable final User testUser = userService.findByLogin("test");
            if (testUser == null) throw new UserIsNotExistException();
            initProject("Simple Project", "Simple Test User Project", testUser.getId());
            initTask("Simple Task", "Simple Test User Task", testUser.getId());
        } catch (Exception exception) {
            loggerService.error(exception);
        }
    }

    @Override
    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    /**
     * Application Main method with infinite loop.
     * @param args Commandline arguments.
     */
    public void run(@NotNull final String... args) {
        System.out.println();
        try {
            if (areArgExists(args)) return;
        } catch (Exception exception) {
            loggerService.error(exception);
            return;
        }

        loggerService.info("****** WELCOME TO TASK MANAGER APPLICATION ******");
        System.out.println("Please, use \"help\" command to see all available commands.\n");
        System.out.println("Please, login/register new user to start work with the application.\n");

        @NotNull final List<String> availableStartupCommands =
                Arrays.asList("register", "login", "logoff", "help", "exit");
        @NotNull String commandName;
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.print("Please, enter your command: ");
            commandName = INPUT.nextLine();
            System.out.println();
            if (isEmpty(commandName)) continue;
            if (authService.isSessionInactive()) {
                if (!availableStartupCommands.contains(commandName)) {
                    System.out.println("Session is inactive! Please, register new user or login.");
                    continue;
                }

                try {
                    loggerService.command(commandName);
                    commandService.getCommandByName(commandName).execute();
                } catch (Exception exception) {
                    loggerService.error(exception);
                }
            }
            else {
                try {
                    @NotNull final AbstractCommand command = commandService.getCommandByName(commandName);
                    if (command.getRoles().contains(authService.getCurrentUserRole())) {
                        loggerService.command(commandName);
                        command.execute();
                        continue;
                    }
                    throw new AccessDeniedException();
                } catch (Exception exception) {
                    loggerService.error(exception);
                }
            }
        }
    }

    private void registerCommand(@NotNull final AbstractCommand command) {
        commandService.add(command);
    }

    private void initProject(
            @NotNull final String projectName, @NotNull final String projectDescription, @NotNull final String userId
            ) {
        projectService.create(new Project(projectName, projectDescription, userId));
    }

    private void initTask(
            @NotNull final String taskName, @NotNull final String taskDescription, @NotNull final String userId
    ) {
        taskService.create(new Task(taskName, taskDescription, userId));
    }

    private boolean areArgExists(@NotNull final String... args) throws Exception {
        if (isEmpty(args)) return false;
        @NotNull final String arg = args[0];
        if (isEmpty(arg)) return false;
        commandService.getArgumentByName(arg).execute();
        return true;
    }

}
